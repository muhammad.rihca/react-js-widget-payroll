import './Payroll.css'

function Payroll(props) {
    const data = props.data
    const i = props.i

    return(
        <tr>
            <th scope="row">{i + 1}</th>
            <td>{data.bulan}</td>
            <td>{data.pajak}</td>
            <td>{data.bruto}</td>
            <td>{data.netto}</td>
        </tr>
    )
}

export default Payroll