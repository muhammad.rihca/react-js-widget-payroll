import Payroll from '../components/Payroll'
import './Penggajian.css'
const data = [
    {
        "bulan": "Januari",
        "pajak": "Rp 3.000.000",
        "bruto": "Rp 30.000.000",
        "netto": "Rp 27.000.000"
    },
    {
        "bulan": "Februari",
        "pajak": "Rp 3.000.000",
        "bruto": "Rp 30.000.000",
        "netto": "Rp 27.000.000"
    },
    {
        "bulan": "Maret",
        "pajak": "Rp 3.000.000",
        "bruto": "Rp 30.000.000",
        "netto": "Rp 27.000.000"
    },
    {
        "bulan": "April",
        "pajak": "Rp 3.000.000",
        "bruto": "Rp 30.000.000",
        "netto": "Rp 27.000.000"
    },
    {
        "bulan": "Mei",
        "pajak": "Rp 3.000.000",
        "bruto": "Rp 30.000.000",
        "netto": "Rp 27.000.000"
    },
    {
        "bulan": "Juni",
        "pajak": "Rp 3.000.000",
        "bruto": "Rp 30.000.000",
        "netto": "Rp 27.000.000"
    },
    {
        "bulan": "Juli",
        "pajak": "Rp 3.000.000",
        "bruto": "Rp 30.000.000",
        "netto": "Rp 27.000.000"
    },
    {
        "bulan": "Agustus",
        "pajak": "Rp 3.000.000",
        "bruto": "Rp 30.000.000",
        "netto": "Rp 27.000.000"
    },
    {
        "bulan": "September",
        "pajak": "Rp 3.000.000",
        "bruto": "Rp 30.000.000",
        "netto": "Rp 27.000.000"
    },
    {
        "bulan": "Oktober",
        "pajak": "Rp 3.000.000",
        "bruto": "Rp 30.000.000",
        "netto": "Rp 27.000.000"
    },
    {
        "bulan": "November",
        "pajak": "Rp 3.000.000",
        "bruto": "Rp 30.000.000",
        "netto": "Rp 27.000.000"
    },
    {
        "bulan": "Desember",
        "pajak": "Rp 3.000.000",
        "bruto": "Rp 30.000.000",
        "netto": "Rp 27.000.000"
    },
]
function Penggajian() {
    return(
        <div className="Container">
        <div className="Information">
          <h1>Penggajian</h1>
          <table className="table">
            <tbody>
              <tr>
                <td>Nama</td>
                <td>: Muhammad Achir</td>
              </tr>
              <tr>
                <td>Status</td>
                <td>: Belum Menikah</td>
              </tr>
            </tbody>
          </table>
        </div>
        <table className="table">
          <thead>
            <tr>
              <th scope="col">No</th>
              <th scope="col">Bulan</th>
              <th scope="col">Pajak</th>
              <th scope="col">Penghasilan Bruto</th>
              <th scope="col">Penghasilan Netto</th>
            </tr>
          </thead>
          <tbody>
            {
                data.map((d, i) => (
                    <Payroll data={d} i={i}/>
                ))
            }
          </tbody>
        </table>
      </div>
    )
}

export default Penggajian