import './App.css';
import Penggajian from './pages/Penggajian';

function App() {
  return (
    <div className="App">
      <Penggajian />
    </div>
  );
}

export default App;
